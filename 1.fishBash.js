function fishBash(n) {
  if (Number.isInteger(n)) {
    for (let i = 1; i <= n; i++) {
      let output = "";
      if (i % 3 === 0) {
        output = "fish";
      }
      if (i % 5 === 0) {
        output = "bash";
      }
      if (i % 15 === 0) {
        output = "fish bash";
      }
      console.log("i =", i, "output =", output);
    }
  } else {
    console.log("Inputan tidak valid");
  }
}
fishBash(20);
