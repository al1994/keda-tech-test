function reverse(str) {
  str = str.toLowerCase().replace(/\s/g, "");
  let splitString = str.split("").reverse();
  let newStr = splitString.join("");

  if (str === newStr) {
    return true;
  } else {
    return false;
  }
}
console.log(reverse("Kasur ini rusak")); // true
console.log(reverse("hello world")); // false
